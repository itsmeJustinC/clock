class SevenSegment {
    constructor() {
        this.segments = [];
        // The order to add the segments is 1, 7, 2, 3, 6, 4, 5

        // create the individual segments
        this.seg1 = $('<div />').addClass('hori-seg');
        this.seg2 = $('<div />').addClass('vert-seg');
        this.seg3 = $('<div />').addClass('hori-seg');
        this.seg4 = $('<div />').addClass('vert-seg');
        this.seg5 = $('<div />').addClass('hori-seg');
        this.seg6 = $('<div />').addClass('vert-seg');
        this.seg7 = $('<div />').addClass('vert-seg');

        // add segments to the segments array
        this.segments.push(
            this.seg1,
            this.seg2,
            this.seg3,
            this.seg4,
            this.seg5,
            this.seg6,
            this.seg7,
        );

        // create the containers for horizontal and vertical segments
        this.horizontal1 = $('<div />').addClass('horizontal');
        this.vertical1 = $('<div />').addClass('vertical');
        this.horizontal2 = $('<div />').addClass('horizontal');
        this.vertical2 = $('<div />').addClass('vertical');
        this.horizontal3 = $('<div />').addClass('horizontal');

        // create the container for the entire seven segment display
        this.displayContainer = $('<div />').addClass('seg-container');

        // add segments to correct segment container 
        this.horizontal1.append(this.seg1);
        this.vertical1.append(this.seg7, this.seg2);
        this.horizontal2.append(this.seg3);
        this.vertical2.append(this.seg6, this.seg4);
        this.horizontal3.append(this.seg5);

        // add the segment containers to the container for the seven segment display 
        this.displayContainer.append(
            this.horizontal1,
            this.vertical1,
            this.horizontal2,
            this.vertical2,
            this.horizontal3
        );
    }
    
    configureLights(config) {
        for (let i = 0; i < this.segments.length; i++) {
            this.segments[i].removeClass('segment-on');
        }
        for (let i = 0; i < config.length; i++) {
            // the minus 1 is because segs are numbered 1-7 in config so their index in 
            // segments array is just minus 1
            this.segments[config[i]-1].addClass('segment-on');
        }
    }
}