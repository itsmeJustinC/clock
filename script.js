// create the 6 displays
let hour1 = new SevenSegment();
let hour2 = new SevenSegment();
let minute1 = new SevenSegment();
let minute2 = new SevenSegment();
let second1 = new SevenSegment();
let second2 = new SevenSegment();

// the intial code
function doubleDigit(num) {
    if (num < 10) {
        return "0" + num;
    }
    return num.toString();
}

function setTime(date) {
    let hour = doubleDigit(date.getHours());
    let minute = doubleDigit(date.getMinutes());
    let second = doubleDigit(date.getSeconds());

    let hour1Config = hour[0];
    let hour2Config = hour[1];
    let minute1Config = minute[0];
    let minute2Config = minute[1];
    let second1Config = second[0];
    let second2Config = second[1];

    hour1.configureLights(config[hour1Config]);
    hour2.configureLights(config[hour2Config]);
    minute1.configureLights(config[minute1Config]);
    minute2.configureLights(config[minute2Config]);
    second1.configureLights(config[second1Config]);
    second2.configureLights(config[second2Config]);
}   

// create the containers for the hours, minutes, and seconds displays
let hours = $('<div />').addClass('time-container');
let minutes = $('<div />').addClass('time-container');
let seconds = $('<div />').addClass('time-container');

// add the display segments to their containers
hours.append(hour1.displayContainer, hour2.displayContainer);
minutes.append(minute1.displayContainer, minute2.displayContainer);
seconds.append(second1.displayContainer, second2.displayContainer);

// add the display segment containers into the min container
$('div#main-container').append(hours, minutes, seconds);

setTime(new Date());
setInterval(() => {setTime(new Date())}, 1000);
